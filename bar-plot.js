// bar-plot.js
d3.svg.census = d3.svg.census || {};


d3.svg.census.bar = function () {

	var  margin = {top: 10, right: 10, bottom: 20, left: 20}
		,width = 500
		,height = 500
		,padding = 0.5
		,tick_format = null
		,x_accessor = function(d){return d[0];}
		,y_accessor = function(d){return d[1];}
		,x_domain = null
		,y_domain = null
	;

	function bar (selection) {
		selection.each( function(datum, index){

			var data = datum.map(function(d){
				return [x_accessor(d), y_accessor(d)];
			});

			var x_scale = d3.scale.linear()
				.domain( x_domain ? x_domain.call(this) : [0, d3.max(data, function(d) { return d[0];})])
				.range([0, width - margin.left - margin.right]);

			var x_scale_old = this.___x_scale___ || x_scale;
			this.___x_scale___ = x_scale;

			var y_scale = d3.scale.ordinal()
				.domain( y_domain ? y_domain.call(this) : data.map(function(d){ return d[1];}))
				.rangeBands([height - margin.top - margin.bottom, 0], padding);

			var y_scale_old = this.___y_scale___ || y_scale;
			this.___y_scale___ = y_scale;		

			var x_axis = d3.svg.axis()
				.tickFormat(tick_format ? tick_format : null);
			
			var y_axis = d3.svg.axis()
				.orient('left')
				;

			var svg = d3.select(this).selectAll("svg").data([datum]);

			var g = svg.enter().append("svg")
					.attr({width: width, height: height})
					.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")" );

			g.append("g").attr("class", "bars");
			g.append("g").attr("class", "x axis");
			g.append("g").attr("class", "y axis");

			g = svg.select("g");

			var bar = g.select(".bars"). selectAll(".bar")
				.data(data, function(d){return d[0];});

			bar.exit()
				.attr({y: height - margin.top - margin.bottom, height: 0})
				.remove();

			bar.enter().append("rect")
				.attr({
					"class" : "bar"
					,x: 0
					,y: function(d) { return y_scale(d[1]);}
					,width: 0
					,height: y_scale.rangeBand()
				})
				.attr('width', function(d){ return x_scale(d[0]);});
			
			bar.enter().append('text')	
		        .attr('x', function(d) { return x_scale(d[0]) + 5})
		        .attr('y', function(d) { return y_scale(d[1]) + y_scale.rangeBand()/2 + 2})
		        .text(function(d) { return tick_format(d[0]);});

			g.select(".x.axis")
				.attr({
					"class" : "x axis"
					,"transform" : "translate(0, " + (height - margin.top - margin.bottom) + ")"
				})
				//uncomment the below to turn on the x-axis tick marks
				//.call(x_axis.scale(x_scale));

			g.select(".y.axis")
				.attr("class", "y axis")
				.call(y_axis.scale(y_scale));

			g.selectAll(".tick text")
				.attr("transform", "translate(" + 8 + ', -' + ( y_scale.rangeBand()/2 + 8 ) + ")")
				.style('text-anchor', 'start')
				;

		});
	}

	bar.margin = function(d) {
		if (!arguments.length) return margin;
		margin = d;
		return bar;
 	}

 	bar.width = function(d) {
 		if (!arguments.length) return width;
 		width = d;
 		return bar;
 	}

 	bar.height = function(d) {
 		if (!arguments.length) return height;
 		height = d;
 		return bar;
 	}

  	bar.padding = function(d) {
 		if (!arguments.length) return padding;
 		padding = d;
 		return bar;
 	}

 	bar.tickFormat = function(d) {
 		if (!arguments.length) return tick_format;
 		tick_format = d;
 		console.log(tick_format);
 		return bar;
 	}

 	bar.x = function(d) {
 		if (!arguments.length) return x_accessor;
 		x_accessor = d;
 		return bar;
 	}

 	bar.y = function(d) {
 		if (!arguments.length) return y_accessor;
 		y_accessor = d;
 		return bar; 		
 	}

 	bar.xDomain = function(d) {
 		if (!arguments.length) return x_domain;
 		x_domain = d3.functor(d);
 		return bar;
 	}

 	bar.xDomain = function(d) {
 		if (!arguments.length) return y_domain;
 		y_domain = d3.functor(d);
 		return bar;
 	}

 	return bar;

}

d3.svg.census.world_map = function () {

   var width = 1003,
        height = 500;

    var country_pop = {};
    var country_extent = new Array();

    var projection = d3.geo.naturalEarth()
        .scale(180)
        .translate([width / 2, height / 2])
        .precision(1.0);

    var path = d3.geo.path()
        .projection(projection);

    var svg = d3.select("#world-map").append("svg")
        .attr("width", width)
        .attr("height", height);

    var nice_pop = d3.format(',');

    var pop_domain = [5e3, 1e6, 1e7, 1e8, 1e9];

    /*

    zoom with canvas

    http://bl.ocks.org/mbostock/c1c0426d50ca8a9f4c97

    other zoom
    http://www.roadtolarissa.com/deportations/

    http://homes.cs.washington.edu/~jheer//files/zoo/ex/time/multiples.html

    http://bl.ocks.org/iaindillingham/5068071

    http://bl.ocks.org/iaindillingham/5125201

	http://bl.ocks.org/mbostock/4573883

    */
    queue()
        .defer(d3.json, "topo_10.json")
        .defer(d3.json, "fips_pop.json")
        .await(ready);

    function ready(error, world, fips) {

		var d3line2 = d3.svg.line()
                .x(function(d){return d.x;})
                .y(function(d){return d.y;})
                .interpolate("linear");

        if (!error) {
            country_pop = fips;
            country_extent = d3.entries(fips);

            var threshold = d3.scale.threshold()

                //.domain(d3.extent(d3.values(country_extent), function(d) {  return +d.value; }));
                .domain(pop_domain)
                .range(d3.range(6).map(function(i) {
                    return "q" + i + "-5";
                }))
                ;

            var tooltip_scale = d3.scale.linear()
            	.range([1, 3.5])
            	.domain([1,44]);

            var first_time = false;

            function redraw() {
            	hud.selectAll('#tooltip').remove();

                if (!first_time) {
                    map.attr("transform", "translate(" + width + "," + height + " ) scale(1.000005)");
                    first_time = true;
                }
                var scale = d3.event.scale === 1 ? 1.000005 : d3.event.scale;
                var t1 = projection.translate(),
                    t2 = d3.event.translate,
                    t = [t2[0] - (t1[0] - width / 2), t2[1] - (t1[1] - height / 2)];

                map.attr("transform",
                    "translate(" + t + ") " +
                    "scale(" + (scale) + height / 2 + ")"
                );
            }
            svg.append('defs')
                .append('radialGradient')
                .attr('id', 'water')
                .attr('cx', '50%')
                .attr('cy', '50%')
                .attr('r', '50%')
                .attr('fx', '50%')
                .attr('fy', '50%')
                .selectAll('stop')
                .data([{
                    offset: '0%',
                    style: 'stop-color:#F0F8FF;stop-opacity:1'
                }, {
                    offset: '100%',
                    style: 'stop-color:#C6E2FF;stop-opacity:1'
                }])
                .enter().append('stop')
                .attr('offset', function(d) {
                    return d.offset;
                })
                .attr('style', function(d) {
                    return d.style;
                });

            var bg = svg.append("g").call(d3.behavior.zoom().scaleExtent([1.000005, 16.999999]).on("zoom", redraw));

            var hud = svg.append("g").attr('id', 'hud');

            var legend_text = Object.create(pop_domain);
            legend_text.unshift('Data unavailable');

      		hud.append('g').selectAll('rect')
      			.data(threshold.range().map(function(color) {
				      var d = threshold.invertExtent(color);
				      //if (d[0] == null) d[0] = x.domain()[0];
				      //if (d[1] == null) d[1] = x.domain()[1];
				      return d;
				    }))
      			.enter()
      			.append('rect')
      			.attr('x', 10)
      			.attr('width', 10)
      			.attr('y', function(d,i){ return height - (i*12 + 30);} )
      			.attr('height', 10)
      			.attr('class', function(d, i){ return "q" + i + "-5";});


      		hud.append('g').selectAll('text')
      			.data(legend_text)
      			.enter()
      			.append('text')
      			.attr('class', 'legend')
      			.attr('x', 30)
      			.attr('y', function(d,i){ return height - (i*12 + 22);} )
      			.text(function(d,i){ 
      				if (!Number.isInteger(d)){
      					return d;
      				} else {
          				return 'more than ' + nice_pop(d);  
          			} 
      			})

            bg.append("rect")
                .attr("class", "background")
                .attr('id', 'water')
                .attr("width", width)
                .attr("height", height)

            var map = bg.append("g")
                .attr("transform", "translate(0, 0) scale(1)");

            // water background
            map.append("g")
                .attr("class", "countries")

            .selectAll("path")
                .data(topojson.feature(world, world.objects.global_map).features)
                .enter().append("path")
                .attr("class", function(d) {
                    try {
                        return threshold(+country_pop[d.id].pop);
                    } catch (e) {
                        return '';
                    }
                })
                .attr("d", path)
                .on('mouseover', function(d) {
			        //debugger;
			        var mouse = d3.mouse(this.parentElement.parentElement.parentElement.parentElement.parentElement);
			        //console.log(mouse);
			        x = 80;
			        threshold = 120;
			        // scale tooltip for textarea
			        y = tooltip_scale(country_pop[d.id].st_name.length)

			 		no_data = country_pop[d.id].pop === null && typeof country_pop[d.id].pop === 'object' ? true : false;

			        text = [country_pop[d.id].st_name, 'population', nice_pop(country_pop[d.id].pop)];
			       
			        if ( no_data ) {
			        	text = [country_pop[d.id].st_name, 'data unavailable'];					     
			        } 

			        seg_1 = [{x:mouse[0], y:mouse[1]},
			                 {x:mouse[0]+x/6, y:mouse[1]- x/2},
			                 {x:mouse[0]+x/2, y:mouse[1]-x/2}
			        ];
			                    //{x:140, y:100}];                           
            		seg_2 = [{x:mouse[0]+x/2, y:mouse[1]-x/2},
            				{x:mouse[0]-x*y, y:mouse[1]-x/2},
            				{x:mouse[0]-x*y, y:mouse[1]-x*1.5},
            				{x:mouse[0]+x/2, y:mouse[1]-x*1.5}
            		]; 

			        seg_3 = [{x:mouse[0], y:mouse[1]},
			                    {x:mouse[0]-x/6, y:mouse[1]+ x/2},
			                    {x:mouse[0]-x/2, y:mouse[1]+x/2}
			        ];
			                    //{x:140, y:100}];                           
            		seg_4 = [{x:mouse[0]-x/2, y:mouse[1]+x/2},
            				{x:mouse[0]+x*y, y:mouse[1]+x/2},
            				{x:mouse[0]+x*y, y:mouse[1]+x*1.5},
            				{x:mouse[0]-x/2, y:mouse[1]+x*1.5}
            		]; 

            		// flip if near top of container
            		text_x_offset = mouse[1] < threshold ? mouse[0]-x+x/2+15 : mouse[0]-x*y+15;
            		text_y_offset = mouse[1] < threshold ? mouse[1]+x-35 : mouse[1]-x-35;
            		tt_arrow_path = mouse[1] < threshold ? seg_3 :  seg_1;
            		tt_main_path  = mouse[1] < threshold ? seg_4 : seg_2;

                	hud.selectAll('#tooltip').remove();

                	var tooltip = hud.append('g').attr('id', 'tooltip');

               		tooltip.append("svg:path")
               			.attr('id', 'tooltip-arrow')
    					.attr("d", d3line2(tt_arrow_path) + 'Z')
    					;

               		tooltip.append("svg:path")
               			.attr('id', 'tooltip-main')
    					.attr("d", d3line2(tt_main_path) + 'Z')
    					;
    				
    				tooltip.append("svg:text")
        				.attr({x:text_x_offset, y:text_y_offset})
        				.selectAll('tspan')
        				.data(text)
        				.enter()
        				.append('tspan')
        				.attr({x:text_x_offset, dy:"1.5em"})
        				.text(function(d){return d;});
                    
                    //debugger;

                    //console.log(this.classList + ' ' + country_pop[d.id].st_name + ' ' + parseInt(country_pop[d.id].pop).toExponential());
                })
                .on('mouseout', function(d) {
                    //tooltip.style('display', 'none');
                   hud.selectAll('#tooltip').remove();
                });

            /*
					  map.append("path")
					      .datum(topojson.mesh(world, world.objects.global_map, function(a, b) { return a !== b; }))
					      .attr("class", "states")
					      .attr("d", path);
					*/

            //console.log(country_extent);
            /*
					 	console.log(country_extent.sort(function(x,y){
					 		return y['value'].pop - x['value'].pop;  
					 	}));
						*/
            var table = d3.select("#most-populous").append("ul").selectAll("li").data(country_extent.sort(function(x, y) {
                    return y['value'].pop - x['value'].pop;
                }).slice(0, 10)).enter()
                .append("li")
                .html(function(d, i) {
                    return '<div><span>' + (i + 1) + '. </span>' + d['value'].st_name + '</div><div>' + nice_pop(d['value'].pop) + '</div>';
                });
        }
    }

}