var  gulp = require('gulp')
	,request = require('request')
    ,fs = require('fs')	
	,base_url = "http://api.census.gov/data/restricted/timeseries/idb/5year?get=POP&YR=2015&FIPS={{KEY}}&key=320bade8beb598810613535949a68b4b1b54f4b7"
	,input_data = "data.json"
	,output_data = {}
	,output_target = 'fips_pop.json'
	;


require('./LZW');

var getJsonFromJsonP = function (url, callback) {
    request(url, function (error, response, body) {
      if (!error && response.statusCode == 200) {
        var jsonpData = body;
        var json;
        //if you don't know for sure that you are getting jsonp, then i'd do something like this
        try
        {
           json = JSON.parse(jsonpData);
        }
        catch(e)
        {
            var startPos = jsonpData.indexOf('({');
            var endPos = jsonpData.indexOf('})');
            var jsonString = jsonpData.substring(startPos+1, endPos+1);
            json = JSON.parse(jsonString);
        }
        callback(null, json);
      } else {
        callback(error);
      }
    })
}


gulp.task('populate', function(){
	fs.readFile( input_data , function (err, data) {
	  if (err) throw err;
	  JSON.parse(data).forEach(function(d, i, a){
		if (d.ST_FIPS_Code.length > 0 ) {
			getJsonFromJsonP(base_url.split('{{KEY}}').join(d.ST_FIPS_Code), function (err, data) {
				if( !!data ){
					output_data[d.ST_FIPS_Code] = {};
					output_data[d.ST_FIPS_Code].pop = data[1][0];
					output_data[d.ST_FIPS_Code].st_name = d["ST_Short-form_Name"];

				} else {
					output_data[d.ST_FIPS_Code] = {};
					output_data[d.ST_FIPS_Code].pop = -Infinity;
					output_data[d.ST_FIPS_Code].st_name = d["ST_Short-form_Name"];

				}
				 if (i === a.length - 1) {
             			require('fs').writeFile(output_target, JSON.stringify(output_data));
        		 }        
			});

		}	  	
	  
	  });
	});
});


gulp.task('compress', function(){


//console.log(fs.readFile('./topo_10.json'));

require('fs').writeFile('topo.min.json', JSON.stringify(LZW.compress(fs.readFileSync('./topo_11.json', 'utf8'))));


})



gulp.task('default', ['populate']);